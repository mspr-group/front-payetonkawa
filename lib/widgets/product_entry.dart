import 'dart:math';

import 'package:paye_ton_kawa/ar.dart';
import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/widgets/animated_folding_widget.dart';
import 'package:paye_ton_kawa/widgets/product_entry_header.dart';
import 'package:paye_ton_kawa/widgets/product_entry_section.dart';
import 'package:paye_ton_kawa/widgets/product_entry_summary.dart';

import '../models/Product.dart';

class ProductEntry extends StatefulWidget {
  final Product product;
  const ProductEntry({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  State<ProductEntry> createState() => _ProductEntryState();
}

class _ProductEntryState extends State<ProductEntry>
    with SingleTickerProviderStateMixin {
  double height = 180;
  late final AnimationController _controller;
  final Duration duration = const Duration(milliseconds: 1000);

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: duration);
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _toggleAnimation() {
    if (_controller.status == AnimationStatus.completed) {
      _controller.reverse();
    } else {
      _controller.forward();
    }
  }

  Widget _senderSection() {
    return ProductEntrySectionRow(
      height: 75,
      padding:
          const EdgeInsets.only(top: 5.0, bottom: 2.5, left: 10.0, right: 10.0),
      children: [
        ProductEntrySection(
          title: 'Vendeur',
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  child: Image.asset('assets/images/avatar.png', height: 45.0),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.product.name,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: [
                        for (int i = 0; i < 5; i++)
                          Icon(
                            i < 3 ? Icons.star : Icons.star_border,
                            color: i < 3 ? Colors.deepPurple : Colors.black54,
                          )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _addressSection() {
    return ProductEntrySectionRow(
      height: 75,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.5),
      children: [
        ProductEntrySection(
          title: 'Description',
          children: [
            Text(
              widget.product.description,
            )
          ],
        ),
      ],
    );
  }

  /*
  Widget _deliverySection() {
    return const ProductEntrySectionRow(
      height: 80,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.5),
      children: [
        ProductEntrySection(
          title: 'Date de livraison',
          children: [
            Text(
              '18:30',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('10 Juillet 2022'),
          ],
        ),
        ProductEntrySection(
          title: 'Mise en vente',
          children: [
            Text(
              'Depuis 24 minutes',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ],
    );
  }
*/
  Widget _messageSection() {
    return Container(
      height: 75,
      width: double.infinity,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(
            top: 2.5, bottom: 5.0, left: 10.0, right: 10.0),
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, AugmentedRealityView.id);
          },
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
              const Color.fromRGBO(28, 103, 88, 1),
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          child: Text(
            'Voir en Réalité Virtuel'.toUpperCase(),
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _toggleAnimation,
      child: AnimatedFoldingWidget(
        animation: Tween(begin: 0.0, end: 1.0).animate(
          CurvedAnimation(
            parent: _controller,
            curve: const Interval(
              0.0,
              1 / 3,
              curve: Curves.easeInOut,
            ),
          ),
        ),
        behind: ProductEntryHeader(
          height: 180,
          product: widget.product,
        ),
        front: ProductEntrySummary(
          product: widget.product,
        ),
        next: Column(
          children: [
            _senderSection(),
            AnimatedFoldingWidget(
              animation: Tween(begin: 0.0, end: 1.0).animate(
                CurvedAnimation(
                  parent: _controller,
                  curve: const Interval(
                    1 / 3,
                    1 / 3 * 2,
                    curve: Curves.easeInOut,
                  ),
                ),
              ),
              behind: _addressSection(),
              front: Container(
                height: 75,
                color: Colors.white,
              ),
              next: AnimatedFoldingWidget(
                animation: Tween(begin: 0.0, end: 1.0).animate(
                  CurvedAnimation(
                    parent: _controller,
                    curve: const Interval(
                      1 / 3 * 2,
                      1.0,
                      curve: Curves.easeInOut,
                    ),
                  ),
                ),
                behind: _messageSection() /*_deliverySection()*/,
                front: Container(
                  height: 75,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
