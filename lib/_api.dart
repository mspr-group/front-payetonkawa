import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:async';

// "https://resellersapi.herokuapp.com/resellers/products/"
const baseUrl = 'https://resellersapi-prod.herokuapp.com/resellers/products/';

class API {
  static Future getProducts(token) {
    var url = baseUrl;
    return http.get(
      Uri.parse(url),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ' + token,
      },
    );
  }
}
