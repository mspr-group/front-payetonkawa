// ignore_for_file: prefer_collection_literals

import 'dart:async';
import 'dart:convert';
import 'package:paye_ton_kawa/widgets/product_list.dart';

import '_api.dart';
import 'package:paye_ton_kawa/models/Product.dart';
import 'package:flutter/material.dart';

class Products extends StatefulWidget {
  static const String id = 'productlist_screen';
  @override
  createState() => _ProductsState();
}

class _ProductsState extends State {
  List<Product> products = [];

  initState() {
    super.initState();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    final Object? code = ModalRoute.of(context)?.settings.arguments;
    String codeBar = code.toString();
    _getProducts(codeBar);
    return Scaffold(
        backgroundColor: const Color.fromRGBO(238, 242, 230, 1),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(28, 103, 88, 1),
          title: const Text('Liste des produits'),
        ),
        body: ProductList(products: products));
  }

  _getProducts(token) {
    API.getProducts(token).then((response) {
      if (response.statusCode != 200) {
        Navigator.pop(context);
      } else {
        setState(() {
          Iterable list = json.decode(response.body);
          products = list.map((model) => Product.fromJson(model)).toList();
        });
      }
    });
  }

  myFunction(a) {
    Timer(Duration(seconds: 10), () {
      if (a == '[]') {
        Navigator.pop(context);
      }
    });
  }
}
