class Product {
  int id;
  String name;
  String productName;
  int stock;
  String description;
  double price;
  String color;

  Product({
    required this.id,
    required this.name,
    required this.stock,
    required this.description,
    required this.price,
    required this.color,
    required this.productName,
  });
  Product.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        productName = json['libelle'],
        stock = json['stock'],
        description = json['description'],
        price = json['price'],
        color = json['color'];

  Map toJson() {
    return {
      'id': id,
      'name': name,
      'libelle': productName,
      'stock': stock,
      'description': description,
      'price': price,
      'color': color,
    };
  }
}
