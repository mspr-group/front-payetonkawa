import 'dart:convert';

import 'package:flutter/material.dart';
import 'qrcode.dart';
import 'package:http/http.dart' as http;

class Login extends StatefulWidget {
  static const String id = 'login_screen';
  final String avatarImage;
  final void Function() navigatePage;
  Login({
    required this.avatarImage,
    required this.navigatePage,
  });
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  final formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  void onLoginClick() {
    if (formKey.currentState!.validate()) {
      http.post(
        Uri.parse(
            "https://resellersapi-prod.herokuapp.com/resellers/auth/signup"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body:
            jsonEncode(<String, String>{'email': email, 'password': password}),
      );
      debugPrint(email + password);
      Navigator.pushNamed(context, QrCode.id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            decoration: const BoxDecoration(
              color: Color.fromRGBO(28, 103, 88, 1),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 120),
            decoration: const BoxDecoration(
              color: Color.fromRGBO(238, 242, 230, 1),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(37.5),
                topRight: Radius.circular(37.5),
              ),
            ),
            child: Container(
              margin: const EdgeInsets.only(top: 90),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Text(
                    "Authentification",
                    style: TextStyle(
                        fontSize: 19,
                        color: Color.fromRGBO(28, 103, 88, 1),
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 30),
                  const Text(
                    'Vous avez déjà un compte?',
                    style: TextStyle(
                        fontSize: 17,
                        color: Color.fromRGBO(28, 103, 88, 1),
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 60,
                    margin: const EdgeInsets.only(
                        top: 20, left: 30, right: 30, bottom: 10),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          const Color.fromRGBO(28, 103, 88, 1),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, QrCode.id);
                      },
                      child: const Text(
                        "Connexion",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  const Divider(
                    color: Color.fromRGBO(61, 131, 97, 1),
                    thickness: 1,
                    height: 30,
                    indent: 10,
                    endIndent: 10,
                  ),
                  const Text(
                    'Inscription',
                    style: TextStyle(
                        fontSize: 19,
                        color: Color.fromRGBO(28, 103, 88, 1),
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 25.5, right: 22.5, left: 22.5),
                    child: Form(
                      key: formKey,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !value.contains('@') ||
                              !value.contains('.')) {
                            return 'Email invalide';
                          } else {
                            return null;
                          }
                        },
                        onChanged: (value) {
                          debugPrint(emailController.value.text);
                          setState(() {
                            email = value;
                          });
                        },
                        cursorColor: const Color.fromRGBO(61, 131, 97, 1),
                        decoration: const InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(90, 90, 90, 1))),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(61, 131, 97, 1)),
                          ),
                          icon: Icon(
                            Icons.email,
                            color: Color.fromRGBO(28, 103, 88, 1),
                          ),
                          contentPadding: EdgeInsets.all(11.25),
                          hintText: "Email",
                          hintStyle: TextStyle(
                            color: Color.fromRGBO(28, 103, 88, 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 22.5, right: 22.5, left: 22.5),
                    child: TextFormField(
                      controller: passwordController,
                      cursorColor: const Color.fromRGBO(61, 131, 97, 1),
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      },
                      decoration: const InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromRGBO(90, 90, 90, 1),
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromRGBO(61, 131, 97, 1)),
                        ),
                        icon: Icon(
                          Icons.lock,
                          color: Color.fromRGBO(28, 103, 88, 1),
                        ),
                        contentPadding: EdgeInsets.all(11.25),
                        hintText: "Mot de passe",
                        hintStyle: TextStyle(
                          color: Color.fromRGBO(28, 103, 88, 1),
                        ),
                      ),
                      obscureText: true,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 60,
                    margin: const EdgeInsets.only(top: 40, left: 30, right: 30),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          const Color.fromRGBO(28, 103, 88, 1),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                      ),
                      onPressed: () => onLoginClick(),
                      child: const Text(
                        "Inscription",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width / 2 - 50,
              top: MediaQuery.of(context).size.height / 10.1,
            ),
            child: CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage(widget.avatarImage),
            ),
          ),
        ],
      ),
    );
  }
}
